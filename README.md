# 串口与UDP透传工具

#### 介绍
串口和UDP的透传小工具，可通过配置文件选择串口号和网络端口号，运行后可通过串口直接发送UDP数据，也可通过UDP数据直接发送串口数据。转发过程中实时显示转发数据统计信息。

#### 软件架构
软件架构说明


#### 安装教程

1.  默认通过VS2010编译
2.  编译完成后在Debug目录下生成可执行文件

#### 使用说明

1.  编辑_config.ini文件，选择串口号，UDP接收和发送端口
2.  保存配置文件后打开Serial_UDP_Transmission.exe，即可开启UDP和串口透传服务