// Serial_UDP_Transmission.cpp : 定义控制台应用程序的入口点。
//
#include<stdio.h>
#include<stdlib.h>
#include "SimpleUDPLib.h"
#include "libserialport.h"
#include<windows.h>
#include <process.h>

#pragma comment(lib, "libserialport.lib")
#pragma comment(lib, "udplibDLL.lib")

struct sp_port *p;
#define CONFIG_FILE_NAME ".\\_config.ini"
#define APP_NAME "SERIAL_UDP_TRANSMISSION"
char target_udp_port[128] = "127.0.0.1:8888";
char recv_udp_port[128] = "127.0.0.1:7777";
char com_port_name[64] = "COM1";
int baudrate = 57600;
int databits = 8;
int stopbits = 1;

#define MAX_BUFFER_LEN 4096
long udp_send_data_count = 0;
long udp_recv_data_count = 0;
long serial_send_data_count = 0;
long serial_recv_data_count = 0;

int udp_recv_buffer_len = MAX_BUFFER_LEN;
int serial_buffer_len = MAX_BUFFER_LEN;
int udp2serial_interval = 10;
int serial2udp_interval = 10;

int check(enum sp_return result)
{
    /* For this example we'll just exit on any error by calling abort(). */
    char *error_message;
    switch (result) {
    case SP_ERR_ARG:
        printf("Error: Invalid argument.\n");
        abort();
    case SP_ERR_FAIL:
        error_message = sp_last_error_message();
        printf("Error: Failed: %s\n", error_message);
        sp_free_error_message(error_message);
        abort();
    case SP_ERR_SUPP:
        printf("Error: Not supported.\n");
        abort();
    case SP_ERR_MEM:
        printf("Error: Couldn't allocate memory.\n");
        abort();
    case SP_OK:
    default:
        return result;
    }
}

/**
 * 接收UDP数据，转发到串口
 */
void UDP2Serial(void*pdata)
{
    int recvlen = 0;
    while(1)
    {
        char Buf[MAX_BUFFER_LEN] = {0};
        recvlen = udplib_recv("0.0.0.0:0", recv_udp_port, Buf, udp_recv_buffer_len);
        if(recvlen > 0)
        {
            udp_recv_data_count += recvlen;
            check(sp_blocking_write(p, Buf, recvlen, 10));
            serial_send_data_count += recvlen;
        }
        Sleep(udp2serial_interval);
    }
}

/**
 * 接收串口数据，转发到UDP
 */
void Serial2UDP(void*pdata)
{
    int readlen = 0;
    int recvlen = 0;
    while(1)
    {
         char Buf1[MAX_BUFFER_LEN] = {0};
         readlen = check(sp_nonblocking_read(p, Buf1, serial_buffer_len));
         
         if(readlen>0)
         {
             udplib_send("0.0.0.0:0", target_udp_port, Buf1, readlen);
             serial_recv_data_count += readlen;
             udp_send_data_count += readlen;
         }
        //printf("Serial2UDP\n");
        Sleep(serial2udp_interval);
    }
}





int main(int argc,char *argv[])
{
    //配置文件初始化
    DWORD ret = 0;
    GetPrivateProfileStringA(APP_NAME, "target_udp_port", "127.0.0.1:8888", target_udp_port, 128, CONFIG_FILE_NAME);
    GetPrivateProfileStringA(APP_NAME, "recv_udp_port", "127.0.0.1:7777", recv_udp_port, 128, CONFIG_FILE_NAME);
    
    GetPrivateProfileStringA(APP_NAME, "com_port_name", "COM1", com_port_name, 64, CONFIG_FILE_NAME);    
    baudrate = GetPrivateProfileIntA(APP_NAME, "baudrate", 57600, CONFIG_FILE_NAME);
    databits = GetPrivateProfileIntA(APP_NAME, "databits", 8, CONFIG_FILE_NAME);
    stopbits = GetPrivateProfileIntA(APP_NAME, "stopbits", 1, CONFIG_FILE_NAME);
    
    udp_recv_buffer_len = GetPrivateProfileIntA(APP_NAME, "udp_recv_buffer_len", 1024, CONFIG_FILE_NAME);;
    serial_buffer_len = GetPrivateProfileIntA(APP_NAME, "serial_buffer_len", 1024, CONFIG_FILE_NAME);;
    udp2serial_interval = GetPrivateProfileIntA(APP_NAME, "udp2serial_interval", 10, CONFIG_FILE_NAME);;
    serial2udp_interval = GetPrivateProfileIntA(APP_NAME, "serial2udp_interval", 10, CONFIG_FILE_NAME);;

    WritePrivateProfileStringA(APP_NAME, "target_udp_port", target_udp_port, CONFIG_FILE_NAME);
    WritePrivateProfileStringA(APP_NAME, "recv_udp_port", recv_udp_port, CONFIG_FILE_NAME);
    WritePrivateProfileStringA(APP_NAME, "com_port_name", com_port_name, CONFIG_FILE_NAME);
    char tmp[16] = {0};
    sprintf(tmp, "%d", baudrate);
    WritePrivateProfileStringA(APP_NAME, "baudrate", tmp, CONFIG_FILE_NAME);
    sprintf(tmp, "%d", databits);
    WritePrivateProfileStringA(APP_NAME, "databits", tmp, CONFIG_FILE_NAME);
    sprintf(tmp, "%d", stopbits);
    WritePrivateProfileStringA(APP_NAME, "stopbits", tmp, CONFIG_FILE_NAME);

    sprintf(tmp, "%d", udp_recv_buffer_len);
    WritePrivateProfileStringA(APP_NAME, "udp_recv_buffer_len", tmp, CONFIG_FILE_NAME);
    sprintf(tmp, "%d", serial_buffer_len);
    WritePrivateProfileStringA(APP_NAME, "serial_buffer_len", tmp, CONFIG_FILE_NAME);
    sprintf(tmp, "%d", udp2serial_interval);
    WritePrivateProfileStringA(APP_NAME, "udp2serial_interval", tmp, CONFIG_FILE_NAME);
    sprintf(tmp, "%d", serial2udp_interval);
    WritePrivateProfileStringA(APP_NAME, "serial2udp_interval", tmp, CONFIG_FILE_NAME);

    //串口和网络端口初始化
    check(sp_get_port_by_name(com_port_name, &p));
    check(sp_open(p, SP_MODE_READ_WRITE));
    check(sp_set_baudrate(p, baudrate));
    check(sp_set_bits(p, databits));
    check(sp_set_parity(p, SP_PARITY_NONE));
    check(sp_set_stopbits(p, stopbits));
    check(sp_set_flowcontrol(p, SP_FLOWCONTROL_NONE));

    udplib_setblockmode(recv_udp_port, 1);//接收端口
    //send_udp_port 发送端口
    //统计信息初始化

    _beginthread(UDP2Serial, 102400, NULL);
    _beginthread(Serial2UDP, 102400, NULL);

    while(1)
    {
        //打印串口统计数据
        //打印网络统计数据
        
        printf("udp_send_data_count    = %d\n", udp_send_data_count);
        printf("udp_recv_data_count    = %d\n", udp_recv_data_count);
        printf("serial_send_data_count = %d\n", serial_send_data_count);
        printf("serial_recv_data_count = %d\n", serial_recv_data_count);
        
        Sleep(1000);

        system("cls");
    }

	return 0;
}

